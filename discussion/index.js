console.log("Hello World!");

/*
	Objects
		- an object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.

	Creating objects using object literal:
		Syntax:
			let objectName = {
				keyA: valueA,
				keyBL valueB
			}
*/

let student = {
		firstName: "LouiesAronn",
		lastName: "Dela Cerna",
		age: 30,
		studentID: "2022-010101",
		email: ["aronndc@gmail.com", "LouiesAronn.DelaCerna@gmail.com"],
		address: {
			Street: "111 Qwe St.",
			City: "Pasay City",
			Country: "Philippines"
		}
}

console.log("Result from creating an object:");
console.log(student);
console.log(typeof student);

// Creating Objects using Constructor Function
/*
	Create a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	Syntax:
		function objectName(valueA, valueB) {
			this.keyA = valueA
		}

		let variable =new function objectName(valueA, valueB)

		console.log(variable)
			- "this" is a keyword that is used for invoking; it refers to the global object
			- don't forget to add "new" keyword when we are creating the variables
*/

function Laptop(name, manufactureDate) {
	this.name = name
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Asus", 2012);
console.log("Result of creating objects using object constructor:")
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", [2020, 2021]);
console.log("Result of creating objects using object constructor:")
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980)
console.log("Result of creating objects using object constructor:")
console.log(oldLaptop);

// Creating empty object as placeholder
let computer = {}
let myComputer = new Object()
console.log(computer)
console.log(myComputer)

myComputer = {
	name: "Asus",
	manufactureDate: 2012
}

console.log(myComputer);


/*MINI ACTIVITY
	- Create an object constructor function to produce 2 objects with 3 key-value pairs
	- Log the 2 new objects in the console and send SS in our GC.
*/

/*
function Car(name, manufactureDate, model) {
	this.name = name
	this.manufactureDate = manufactureDate
	this.model = model
}

let car1 = new Car("Mitsubishi", 2022, "Moterosport")
console.log("Car details below:")
let car2 = new Car("Toyota", 2022, "Fortuner")


console.log(car1);
console.log(car2);
*/


// Accessing Object Property

// Using the dot notation
// Syntax: objectName.properyName
console.log("Result from dot notation: " + myLaptop.name);

// Using the bracket noation
// Syntax: objectName["name"]
console.log("Result from bracket notation: " + myLaptop["name"]);

// Accessing array objects
let array = [laptop, myLaptop];
// let array = [{name: "Asus", manufactureDate: 2012}, {name: "Macbook Air", manufactureDate: [2020, 2021]}]

// dot notation
console.log(array[0].name);

// Square Bracket Notation
console.log(array[0]["name"]);

// Initializing / Adding / Deleting / Reassigning Object Properties

let car = {}
console.log(car);

//  Adding object properties
car.name = "Honda Civic"
console.log("Result from adding properties using doe notation:")
console.log(car);

car["manufactureDate"]= 2019
console.log(car);

car.name = ["Ferrari", "Toyota"];
console.log(car)



// Deleting objecting properties
delete car["manufactureDate"];
console.log("Result from deleting object properties:");
console.log(car);

// Reassign objects Properties
car.name = "Tesla"
console.log("Result from reassigning object property:")
console.log(car);

/* 
	Object Method
		a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are function related to a specific object property.

*/


let person = {
	name: "John",
	talk: function() {
		console.log("Hello! My name is " + this.name + ".");
	}
}

console.log(person);
console.log("Result from object methods:")
person.talk();

person.walk = function() {
	console.log(this.name + " walked 25 steps forward.")
}

person.walk();

let friend = {
	firstName: "John",
	lastName: "Doe",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["john.doe@gmail.com", "jd@gmail.com"],
	introduce: function() {
		console.log("Hello! My name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()

// Real World Application

/*
	Scenario:
		1. We would like to create a game that would have several pokemon to interact with each other.
		2. Every Pokemon would have the same sets of stats, properties annd functions.
*/

// Using Object Literals
let myPokemon = {
  name: "Pikachu",
  level: 3,
  health: 100,
  attack: 50,
  tackle: function () {
    console.log("This pokemon takcled tartetPokemon");
    console.log("tartetPokemon's health is now reduced to targetPokemonHealth");
  },
  faint: function () {
    console.log("Pokemon fainted");
  },
};

console.log(myPokemon);

// Using Object Constructor
function Pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = 3 * level;
  this.attack = 2 * level;

  // Methods
  (this.tackle = function(target) {
      console.log(this.name + " tackled " + target.name);
      console.log(
          target.name + "'s health is now reduced " + (target.health - this.attack)
      );
      if (target.health - this.attack <= 5) {
          target.faint();
      }
      target.health = target.health - this.attack;
      console.log(target);
  }),
  (this.faint = function() {
      console.log(this.name + " fainted");
  });
  }

let charizard = new Pokemon("Charizard", 12);
let squirtle = new Pokemon("Squirtle", 6);

console.log(charizard);
console.log(squirtle);


//MAIN ACTIVITY:

charizard.tackle(squirtle);


console.log(" ");
console.log(" ");
console.log(" ");


let trainer = {
    name: "Ash Ketchum",
    age: 15,
    pokemon: ["Pikachu", "Squirtle", "Bulbasaur", "Charmander"],
    talk: function() {
        console.log("Pikachu! I choose you!");
    },
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"],
    },
};

console.log(trainer);
console.log(trainer.name);
console.log(trainer["pokemon"]);
console.log(trainer.talk());

let pikachu = new Pokemon("Pikachu", 12);
let charmander = new Pokemon("Charmander", 8);
let bulbasaur = new Pokemon("Bulbasaur", 6);

console.log(pikachu);
console.log(charmander);
console.log(bulbasaur);

console.log(charmander.tackle(pikachu));
console.log(charmander.tackle(pikachu));

console.log(charmander.tackle(bulbasaur));
